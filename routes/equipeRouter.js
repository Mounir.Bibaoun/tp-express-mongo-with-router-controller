import { Router } from "express";
import equipeController from '../controller/equipeController.js';



const equipeRouter = Router();

// GET all teams
equipeRouter.get('/', equipeController.getAllTeams);

// GET a team by id
equipeRouter.get('/:id', equipeController.getTeamById);

// Add a new team
equipeRouter.post('/', equipeController.addNewTeam);

// Delete a team
equipeRouter.delete('/:id', equipeController.deleteTeamById);

// Update a team
equipeRouter.put('/:id', equipeController.updateTeamById);

export default equipeRouter;
