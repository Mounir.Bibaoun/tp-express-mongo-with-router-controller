import { Router } from "express";
import joueurController from "../controller/joueurController.js";



const joueurRouter=Router();

//get all players
joueurRouter.get('/',joueurController.getAll);
//get a player by id
joueurRouter.get('/:id',joueurController.getById);
//add new player
joueurRouter.post('/',joueurController.addNewPlayer);
//delete a player
joueurRouter.delete('/:id',joueurController.deletePlayer);
//update a player
joueurRouter.put('/:id',joueurController.updatePlayer);

joueurRouter.get('/joueursequipe/:eqId', joueurController.getJoueursEquipeById);





export default joueurRouter;