import { request } from "express";
import Joueur from "../model/joueur.js";

const getAll = (request, response) => {
    Joueur.find()
        .populate('equipe')
        .then(joueurs => response.status(200).json(joueurs))
        .catch(err => {
            console.log(err);
            response.status(500).json({
                message: 'Cannot get the players list '
            });
        });
};

const getById = (request, response) => {
    const id = request.params.id;
    Joueur.findById(id)
        .populate('equipe')
        .then(joueur => {
            if (joueur) {
                response.status(200).json(joueur);
            } else {
                response.status(404).json({ message: `Player with id ${id} not found` });
            }
        })
        .catch(err => response.status(500).json({ message: `Can not find a player with id: ${id}` }));
};
const addNewPlayer =(request,response)=>{
    const {name,post,num,equipe}=request.body;
  const joueur=new Joueur({name,post,num,equipe});
  joueur.save()
     .then(jo=>{
      console.log(`player  ${jo.name} creaded`);
      response.status(201).json({message:`player  ${jo.name} creaded`})
     }).catch(err=>{
      console.log(`erreur ${err}`);
      response.status(500).json({message:err.message});
     })
};


const deletePlayer = (request,response)=>{
    const id=request.params.id;
    Joueur.deleteOne({
     _id:id
    })
    .then(()=>{
      console.log(`player with id =  ${id} was deleted`);
        response.status(200).json({message:`player  with id = ${id} was deleted`});
    })
    .catch(err=>{
        console.log(`erreur ${err}`);
        response.status(500).json({message:`Can not delete the player with id ${id}`});
    })

};
const updatePlayer = (request,response)=>{
    const id=request.params.id;
    const {name,post,num,equipe}=request.body;
    Joueur.updateOne({_id:id},{name,post,num,equipe})
         .then(jo=>{
                console.log(`player  ${jo.name} updated`);
             response.status(200).json({message:`player  ${id} updated`})
         })
         .catch(err=>{
        console.log(`erreur ${err}`);
        response.status(500).json({message:`Can not update the player with id ${id}`});
    })

};
const getJoueursEquipeById = (request, response) => {
    const eqId = request.params.eqId;
    Joueur.find({ equipe: eqId })
        .populate('equipe')
        .then(data => response.status(200).json(data))
        .catch(err => response.status(500).json({ message: err }));
};

export default { getAll, getById, addNewPlayer, deletePlayer, updatePlayer,getJoueursEquipeById };
